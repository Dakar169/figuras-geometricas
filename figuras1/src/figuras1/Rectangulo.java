/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras1;

/**
 *
 * @author sigmotoa
 */
public class Rectangulo extends FiguraGeometrica{
    private double base, altura,longitud;

    public Rectangulo(double base, double altura, double longitud) {
        super("Rectangulo");
        this.base = base;
        this.altura = altura;
        this.longitud = longitud;        
    }

public double area()
{
    return base*altura;
}

public double perimetro(){
    return (base*2)+(altura*2);
}

public double volumen(){
    return base*altura*longitud;
}

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }
    
    //
}
