/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras1;

/**
 *
 * @author sigmotoa
 */
public abstract class FiguraGeometrica {
    
    private String nombre;

    public FiguraGeometrica(String nombre) {
        this.nombre = nombre;
    }
    
    public abstract double area();
    public abstract double perimetro();
    public abstract double volumen();
    
    public String toString ()
    {
        return "El área del "+nombre+" es: "+area()+"\n"
                + "El perímetro del "+nombre+ " es: "+perimetro()+"\n"
                + "El volumen del " +nombre+ " es: "+volumen();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
            
    
     //    
    
    
}